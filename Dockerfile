FROM ruby:3.2
ADD . /app
WORKDIR /app
RUN bundle install
EXPOSE 4567
CMD ["ruby", "web.rb"]

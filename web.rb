#!/usr/bin/env ruby

require 'sinatra'
require 'pp'
require 'json'
require './metrics.rb'

set :bind, '0.0.0.0'

$stdout.sync = true

ac_host="192.168.0.32"


get '/metrics' do
  # Needed for Prometheus v3
  headers 'Content-Type' => 'text/plain; version=0.0.4; charset=utf-8'
  metrics(ac_host)
end


require 'net/http'

def metrics(ac_host)
  sensor_info=Net::HTTP.get(ac_host, '/aircon/get_sensor_info')
  sensor={}
  sensor_info.split(',').each do |s|
   k,v = s.split('=')
   sensor[k]=v
  end
  control_info=Net::HTTP.get(ac_host, '/aircon/get_control_info')
  control={}
  control_info.split(',').each do |s|
   k,v = s.split('=')
   control[k]=v
  end
  weekly_usage=Net::HTTP.get(ac_host, '/aircon/get_week_power')
  usage={}
  weekly_usage.split(',').each do |s|
   k,v = s.split('=')
   usage[k]=v
  end



  real_fan_rate = (control['f_rate'].to_i - 2)* control['pow'].to_i
  real_target_temp = control['stemp'].to_i * control['pow'].to_i

  real_fan_rate = 6 if control['f_rate'] == "A"
  real_fan_rate = 7 if control['f_rate'] == "B"

  output = <<~METRICS 
  # HELP daikin_inside_temp Inside temperature
  # TYPE daikin_inside_temp gauge
  daikin_inside_temp #{sensor['htemp']}
  # HELP daikin_outside_temp Outside temperature
  # TYPE daikin_outside_temp gauge
  daikin_outside_temp #{sensor['otemp']}
  # HELP daikin_power_state Power state
  # TYPE daikin_power_state gauge
  daikin_power_state #{control['pow']}
  # HELP daikin_mode Operation mode
  # TYPE daikin_mode gauge
  daikin_mode #{control['mode']}
  # HELP daikin_today_runtime Todays run time
  # TYPE daikin_today_runtime gauge
  daikin_today_runtime #{usage['today_runtime']}
  # HELP daikin_real_fan_rate Actual fan rate allowing for power state
  # TYPE daikin_real_fan_rate gauge
  daikin_real_fan_rate #{real_fan_rate}
  # HELP daikin_real_target_temp Actual target temperature allowing for power state
  # TYPE daikin_real_target_temp gauge
  daikin_real_target_temp #{real_target_temp}
  METRICS

  output
end
